package at.ffesternberg.dsgvosplitter;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentInformation;

import java.io.*;
import java.util.List;

public class DSGVOPdfSplitter {

    private PDDocument srcPdf;
    private PDDocumentInformation docInfo;

    /**
     * Loads the PDF Sourcefile to split
     *
     * @param sourcefile
     * @throws IOException
     */
    public DSGVOPdfSplitter(File sourcefile) throws IOException {
        srcPdf = PDDocument.load(sourcefile);
        docInfo = new PDDocumentInformation();
        docInfo.setCreator("DSGVO PDF Splitter by Florian Huber, FF Esternberg/AFKDO Engelhartszell");
        docInfo.setAuthor("Splitted from " + sourcefile.getName());
    }

    /**
     * Extracts a document from the loaded PDF.
     *
     * @param document
     * @param output
     * @throws IOException
     */
    public void extractDocument(DSGVODocument document, OutputStream output) throws IOException {
        PDDocument newDoc = new PDDocument();
        for (int page : document.getPages()) {
            newDoc.addPage(srcPdf.getPage(page));
        }
        newDoc.save(output);
        newDoc.close();
    }

    public void splitToFolder(List<DSGVODocument> documents, File outDir) throws IOException {
        if (!outDir.isDirectory()) {
            outDir.mkdirs();
        }
        for (DSGVODocument doc : documents) {
            FileOutputStream fos = new FileOutputStream(new File(outDir, doc.getId() + ".pdf"));
            extractDocument(doc, fos);
        }
    }
}
