package at.ffesternberg.dsgvosplitter;

import java.util.ArrayList;
import java.util.List;

public class DSGVODocument {

    private String id;
    private final List<Integer> pages = new ArrayList<>();


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        if (id == null) {
            return null;
        }
        return getId().substring(6);
    }


    @Override
    public String toString() {
        String ret = "DSGVODocument: id:" + id + " pages: ";
        for (int page:getPages()){
            ret+=page+", ";
        }
        return ret.substring(0, ret.length()-2);
    }

    public List<Integer> getPages() {
        return pages;
    }

}
