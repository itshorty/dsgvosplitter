package at.ffesternberg.dsgvosplitter.cli;

import at.ffesternberg.dsgvosplitter.DSGVODocument;
import at.ffesternberg.dsgvosplitter.DSGVOPdfAnalyzer;
import at.ffesternberg.dsgvosplitter.DSGVOPdfSplitter;
import at.ffesternberg.dsgvosplitter.sybosclient.DSGVOSybosClient;
import org.apache.commons.cli.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;

public class DSGVOSplitterCLI {
    private static final Logger log = LoggerFactory.getLogger(DSGVOSplitterCLI.class);

    public static void main(String[] args) throws Exception {
        Options options = new Options();
        options.addRequiredOption("s", "src", true, "Source PDF");
        options.addOption("o", "out", true, "Output Folder - current if non given");
        options.addOption("u", "user", true, "User for SyBOS upload");
        options.addOption("p", "password", true, "Password for SyBOS Upload");
        options.addOption(null, "dryrun", false, "Do not Split or Upload file");

        CommandLineParser clp = new DefaultParser();
        CommandLine cmd = null;
        try {
            cmd = clp.parse(options, args);
        } catch (ParseException px) {
            printHelp(options);
        }
        if ((cmd.hasOption("u") && !cmd.hasOption("p"))
                || (cmd.hasOption("p") && !cmd.hasOption("u"))) {
            printHelp(options);
        }


        String srcFileName = cmd.getOptionValue("s");
        String outFileName = cmd.getOptionValue("o");
        String username = cmd.getOptionValue("u");
        String password = cmd.getOptionValue("p");
        boolean dryrun = cmd.hasOption("dryrun");
        File sourcePdf = new File(srcFileName);

        DSGVOPdfAnalyzer analyzer = new DSGVOPdfAnalyzer(sourcePdf);
        analyzer.analyze();
        DSGVOPdfSplitter splitter = new DSGVOPdfSplitter(sourcePdf);

        if (outFileName != null && !outFileName.isEmpty()) {
            File outdir = new File(outFileName);

            if (!sourcePdf.isFile() || !sourcePdf.canRead()) {
                throw new IllegalArgumentException("Can't read source pdf file " + srcFileName);
            }

            if (!outdir.isDirectory() || !outdir.canWrite()) {
                throw new IllegalArgumentException("Can't find or write to outdir " + outFileName);
            }

            if (!dryrun)
                splitter.splitToFolder(analyzer.getDocuments(), outdir);
        }
        if (username != null && !username.isEmpty()) {
            DSGVOSybosClient sybosClient = new DSGVOSybosClient(username, password);

            for (DSGVODocument doc : analyzer.getDocuments()) {
                ByteArrayOutputStream bos = new ByteArrayOutputStream();

                splitter.extractDocument(doc, bos);

                ByteArrayInputStream bis = new ByteArrayInputStream(bos.toByteArray());
                if (!dryrun)
                    sybosClient.uploadDocument(doc, bis);
                bos.close();
                bis.close();
            }

        }
    }

    private static void printHelp(Options options) {
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("java -jar dsgvo-uploader.jar", options);
        System.exit(-255);
    }
}
