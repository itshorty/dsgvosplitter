package at.ffesternberg.dsgvosplitter;

import com.google.zxing.*;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.common.HybridBinarizer;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.ImageType;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DSGVOPdfAnalyzer {
    private static final Logger log = LoggerFactory.getLogger(DSGVOPdfAnalyzer.class);

    private File srcPdfFile;
    private List<DSGVODocument> documents = null;


    public DSGVOPdfAnalyzer(File srcPdfFile) {
        this.srcPdfFile = srcPdfFile;
        if (srcPdfFile == null) {
            throw new NullPointerException("srcPdfFile is null");
        }
    }


    public List<DSGVODocument> getDocuments() {
        return documents;
    }

    public void analyze() throws IOException {
        PDDocument srcPdf = null;
        try {
            documents = new ArrayList<>();
            srcPdf=PDDocument.load(srcPdfFile);
            PDFRenderer pdfRenderer = new PDFRenderer(srcPdf);
            MultiFormatReader mfr = setupMultiFormatReader();

            log.info("Loaded srcPdfFile " + srcPdfFile.getAbsolutePath());
            log.info(srcPdf.getNumberOfPages() + " Pages to analyze...");

            DSGVODocument currentDoc = null;

            for (int page = 0; page < srcPdf.getNumberOfPages(); page++) {
                log.info("Analyzing Page: " + (page + 1) + "/" + srcPdf.getNumberOfPages());
                BinaryBitmap bb = renderPagetoBitmap(pdfRenderer, page);
                try {
                    Result result = mfr.decodeWithState(bb);
                    // We found a new Document!
                    if (currentDoc != null) {
                        // Save previous Doc
                        documents.add(currentDoc);
                    }
                    log.info("Found Document - " + result.getText());
                    currentDoc = new DSGVODocument();
                    currentDoc.setId(result.getText());
                    currentDoc.getPages().add(page);
                } catch (NotFoundException e) {
                    if (currentDoc != null) {
                        log.debug("No Barcode found on page " + page + " must be part of Doc " + currentDoc);
                        currentDoc.getPages().add(page);
                    } else {
                        log.warn("No Barcode found on Page " + page + " Preceeding pages?");
                    }
                }
            }
            if (currentDoc != null) {
                documents.add(currentDoc);
            } else {
                log.error("No Documents found!");
            }
        } finally {
            if(srcPdf!=null) srcPdf.close();
        }
    }


    private BinaryBitmap renderPagetoBitmap(PDFRenderer pdfRenderer, int page) throws IOException {
        BufferedImage bim = pdfRenderer.renderImageWithDPI(page, 200, ImageType.BINARY);
        BufferedImageLuminanceSource bimls = new BufferedImageLuminanceSource(bim);
        HybridBinarizer hb = new HybridBinarizer(bimls);
        return new BinaryBitmap(hb);
    }

    private MultiFormatReader setupMultiFormatReader() {
        MultiFormatReader mfr = new MultiFormatReader();
        // Give the Reader some Hints!
        HashMap<DecodeHintType, Object> decodeHints = new HashMap<DecodeHintType, Object>();
        // Code is always in CODE 128 Format
        List<BarcodeFormat> formats = new ArrayList<BarcodeFormat>();
        formats.add(BarcodeFormat.CODE_128);
        decodeHints.put(DecodeHintType.POSSIBLE_FORMATS, formats);
        // Try it Hard! We have time!
        decodeHints.put(DecodeHintType.TRY_HARDER, Boolean.TRUE);
        mfr.setHints(decodeHints);
        return mfr;
    }

}
