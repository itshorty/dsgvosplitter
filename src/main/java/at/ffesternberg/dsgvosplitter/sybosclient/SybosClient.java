package at.ffesternberg.dsgvosplitter.sybosclient;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;

public abstract class SybosClient {
    private static final Logger log = LoggerFactory.getLogger(SybosClient.class);
    private BasicCookieStore cookieStore = new BasicCookieStore();
    private CloseableHttpClient httpclient;

    public SybosClient(String username, String password) throws IOException {
        httpclient = HttpClients.custom()
                .setDefaultCookieStore(cookieStore)
                .build();
        login(username, password);
    }

    private void login(String username, String password) throws IOException {
        log.info("Login to SyBOS - Username: " + username);
        String formCheckId = loadFormCheckId("https://sybos.ooelfv.at/index.php");


        HttpUriRequest login = RequestBuilder.post()
                .setUri("https://sybos.ooelfv.at/index.php?t=1")
                .addParameter("patFormCheckID", formCheckId)
                .addParameter("patFormAlreadySent", "0")
                .addParameter("patFrmMainAction", "")
                .addParameter("module", "Users")
                .addParameter("action", "Authenticate")
                .addParameter("return_module", "Users")
                .addParameter("return_action", "Login")
                .addParameter("login_module", "Contacts")
                .addParameter("login_action", "index")
                .addParameter("patAction", "")
                .addParameter("username", username)
                .addParameter("password", password)
                .addParameter("Login", "  Login  ")
                .build();
        CloseableHttpResponse response = httpclient.execute(login);
        log.debug("Login form POST: " + response.getStatusLine());
        Header refreshHeader = response.getLastHeader("refresh");
        response.close();

        if (refreshHeader != null && refreshHeader.getValue().equals("0; url=https://sybos.ooelfv.at/portal.php")) {
            log.info("Login successful!");
            return;
        } else {
            log.error("Login failed!");
            throw new IllegalStateException("Error logging in to SyBOS!");
        }

    }

    protected String loadFormCheckId(String url) throws IOException {
        HttpGet preLogin = new HttpGet(url);
        CloseableHttpResponse responseLogin = httpclient.execute(preLogin);
        HttpEntity entity = responseLogin.getEntity();

        String formCheckId = serachFormCheckId(entity.getContent());
        EntityUtils.consume(entity);
        responseLogin.close();
        log.debug("form ID: " + formCheckId);
        return formCheckId;
    }

    protected String serachFormCheckId(InputStream content) {
        Scanner scanner = new Scanner(content);
        String formCheckId = scanner.findWithinHorizon(".*name=\"patFormCheckID\".*", 0);
        formCheckId = formCheckId.replaceAll(".*value=\"([^\"]*)\" />", "$1");
        return formCheckId;
    }

    protected CloseableHttpResponse executeRequest(HttpUriRequest request) throws IOException {
        return httpclient.execute(request);
    }

}
