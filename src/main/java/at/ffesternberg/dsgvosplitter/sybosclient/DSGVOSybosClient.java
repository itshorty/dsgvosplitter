package at.ffesternberg.dsgvosplitter.sybosclient;

import at.ffesternberg.dsgvosplitter.DSGVODocument;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class DSGVOSybosClient extends SybosClient {
    private static final Logger log = LoggerFactory.getLogger(DSGVOSybosClient.class);

    private static final DateFormat sybosDate = new SimpleDateFormat("dd.MM.yyyy");

    public DSGVOSybosClient(String username, String password) throws IOException {
        super(username, password);
    }

    public void uploadDocument(DSGVODocument doc, InputStream pdf) throws IOException {
        String formURL = "https://sybos.ooelfv.at/index.php?comp=sybAdresse&s=Dokument&idParent=" + doc.getUserId() + "&edit=1&patJustContent=1";

        log.info("Uploading Doc " + doc.getId() + " for User " + doc.getUserId());
        String formCheckId = loadFormCheckId(formURL);


        HttpPost uploadRequest = new HttpPost(formURL);

        HttpEntity requestEntity = MultipartEntityBuilder.create()
                .addTextBody("patFormCheckID", formCheckId)
                .addTextBody("patFormAlreadySent", "0")
                .addTextBody("patFrmMainAction", "")
                .addTextBody("STtitel", "DSGVO")
                .addTextBody("STdatum", sybosDate.format(new Date()))
                .addTextBody("DKebeneRead", "750")
                .addTextBody("DKebeneWrite", "750")
                .addTextBody("STdescription", "")
                .addTextBody("action_next", "action_next")
                .addTextBody("patSave", "STtitel;STdatum;DKebeneRead;DKebeneWrite;STdescription;sySTORE_STnr;ADRESSE_ADadrnr;")
                .addTextBody("sySTORE_STnr", "")
                .addTextBody("ADRESSE_ADadrnr", doc.getUserId())
                .addBinaryBody("upload_file[]", pdf, ContentType.APPLICATION_OCTET_STREAM, doc.getId() + ".pdf")
                .build();
        uploadRequest.setEntity(requestEntity);

        CloseableHttpResponse uploadResponse = executeRequest(uploadRequest);

        HttpEntity entity = uploadResponse.getEntity();

        Scanner scanner = new Scanner(entity.getContent());
        scanner.findWithinHorizon("window\\.close\\(\\);", 0);

        EntityUtils.consume(entity);
        uploadResponse.close();

        if (scanner.hasNext()) {
            log.info("Document uploaded!");
            return;
        } else {
            log.error("Error while uploading!");
            throw new IOException("Error while uploading Document " + doc);
        }
    }
}