package at.ffesternberg.dsgvosplitter;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class DSGVOPdfSplitterTest {

    private File srcPdfFile;
    private File outdir;


    @BeforeTest
    public void prepareTest() {
        ClassLoader classLoader = getClass().getClassLoader();
        srcPdfFile = new File(classLoader.getResource("original.pdf").getFile());
        Assert.assertNotNull(srcPdfFile, "Can't find test pdf");
        Assert.assertTrue(srcPdfFile.canRead(), "Can't read test pdf");

        outdir = new File("out");
        outdir.mkdirs();
        System.out.println("Outdir: " + outdir.getAbsolutePath());
        Assert.assertTrue(outdir.isDirectory() && outdir.canWrite());
    }

    private List<DSGVODocument> documents;

    @Test()
    public void testAnalyzer() {
        DSGVOPdfAnalyzer analyzer = new DSGVOPdfAnalyzer(srcPdfFile);
        try {
            analyzer.analyze();
            documents = analyzer.getDocuments();
            for (DSGVODocument document : documents) {
                System.out.println(document);
            }
        } catch (IOException e) {
            Assert.fail("IO Exception", e);
        }

    }

    @Test(dependsOnMethods = "testAnalyzer")
    public void testSplitter() {
        try {
            DSGVOPdfSplitter splitter = new DSGVOPdfSplitter(srcPdfFile);

            splitter.splitToFolder(documents, outdir);
        } catch (IOException e) {
            Assert.fail("IO Exception", e);
        }
    }
}
